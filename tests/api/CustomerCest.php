<?php

use Codeception\Util\HttpCode;

class CustomerCest
{

    public function createCustomer(ApiTester $I)        //Создаем Customer
    {
        $data = [
            'name' => 'Aleksandr',
            'firstname' => 'Sergeevich',
            'lastname' => 'Pushkin',
            'phone' => '+7 812 9011837',
            'email' => 'Pushkin@poet.ru'
        ];

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/api/customer', $data);

//        Так выглядела строка до использования use \Codeception\Util\HttpCode
//        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::CREATED); // response 201

//        Так строка выглядит после подключения \Codeception\Util\HttpCode
        $I->seeResponseCodeIs(HttpCode::CREATED); // 201
        $I->seeResponseIsJson();
        $user_id = $I->grabResponse();
        $file = './tmp/userID.txt';
        file_put_contents($file, mb_substr($user_id, 15, -2));
        $I->seeResponseContains($user_id);
    }

    public function getCustomer(ApiTester $I)
    {
        $user_id = file_get_contents('./tmp/userID.txt');
        $data = [
            'name' => 'Aleksandr',
            'firstname' => 'Sergeevich',
            'lastname' => 'Pushkin',
            'phone' => '+7 812 9011837',
            'email' => 'Pushkin@poet.ru'
        ];

        $I->sendGet('/api/customer/' . $user_id);
        $I->seeResponseCodeIs(HttpCode::OK); // 200
        $I->seeResponseContains(json_encode($data));
    }

    public function updateCustomer(ApiTester $I)
    {
        $user_id = file_get_contents('./tmp/userID.txt');
        $I->sendGet('/api/customer/' . $user_id);

        $changeCustomer = [
            'name' => 'Georges',
            'firstname' => 'Charles',
            'lastname' => 'd\'Anthes',
            'phone' => '+7 812 2111895',
            'email' => 'd\'Anthes@dueliant.fr'
        ];
        $I->seeResponseCodeIs(HttpCode::OK); // 200
        $I->sendPut('/api/customer/' . $user_id, $changeCustomer);
    }

    public function deleteCustomer(ApiTester $I)
    {
        $user_id = file_get_contents('./tmp/userID.txt');
        $I->sendDelete('/api/customer/' . $user_id);
        $I->canSeeResponseCodeIs(HttpCode::OK); // 200
    }

    public function deleteUnrealCustomer(ApiTester $I)
    {
        $I->sendDelete('/api/customer/unrealCustomer');
        $I->canSeeResponseCodeIs(HttpCode::NOT_FOUND); // 404
    }

    public function getUnrealCustomer(ApiTester $I)
    {
        $I->sendGet('/api/customer/unrealCustomer');
        $I->canSeeResponseCodeIs(HttpCode::NOT_FOUND); // 404
    }
}